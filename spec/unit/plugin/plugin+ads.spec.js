describe('Plugin', () => {
  var Plugin = require('../../../src/plugin/plugin')
  var plugin
  var adapter = {
    id: 'adapter',
    on: function () { },
    off: function () { },
    dispose: function () { },
    getMetrics: function () { },
    setIsAds: function () { },
    getPlayhead: function() {return 200},
    flags: {
      isStarted: false,
      isJoined: false,
      isPaused: false,
      isSeeking: false,
      isBuffering: false,
      isEnded: false,
      isStopped: false
    }
  }
  var adsAdapter = {
    id: 'adapter',
    on: function () { },
    off: function () { },
    dispose: function () { },
    getMetrics: function () { },
    setIsAds: function () { },
    stopChronoView: function () {},
    flags: {
      isStarted: false,
      isJoined: false,
      isPaused: false,
      isSeeking: false,
      isBuffering: false,
      isEnded: false,
      isStopped: false,
      reset: function () {}
    },
    chronos: {
      total: {
        getDeltaTime: function() {}
      }
    }
  }

  beforeEach(() => {
    plugin = new Plugin()
    plugin.requestBuilder = {
      lastSent: {},
      buildParams: function () {}
    }
  })

  it('should not be able to send stop, waiting a first postroll', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeFalsy()
  })

  it('should not be able to send stop, waiting a second postroll', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 2
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    plugin.playedPostrolls = 1
    expect(plugin._isStopReady()).toBeFalsy()
  })

  it('should not be able to send stop, waiting for postroll', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeFalsy()
  })

  it('should be able to send stop, postroll happened', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    plugin.playedPostrolls = 1
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should be able to send stop, live', () => {
    plugin.requestBuilder.lastSent.live = true
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should be able to send stop, no ads adapter', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adapter = adapter
    plugin._adsAdapter = null
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should be able to send stop, live', () => {
    plugin.requestBuilder.lastSent.live = true
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.breaksTime = [0,1,200]
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should be able to send stop, no information', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should not be able to send stop, waiting a first postroll with expected', () => {
    plugin.options['ad.expectedPattern'] = {
      post: [1]
    }
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeFalsy()
  })

  it('should be able to send stop, no postrolls in breaktime', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.breaksTime = [0,1,190]
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should be able to send stop, no ads in breaktime', () => {
    plugin.requestBuilder.lastSent.live = false
    plugin.requestBuilder.lastSent.mediaDuration = 200
    plugin.requestBuilder.lastSent.breaksTime = []
    plugin._adapter = adapter
    plugin._adsAdapter = adsAdapter
    expect(plugin._isStopReady()).toBeTruthy()
  })

  it('should not be able to send stop from adstop', () => {
    plugin._adsAdapter = adsAdapter
    var obj = {
      data: {
        params: null
      }
    }
    spyOn(plugin, '_sendAdEventIfAllowed')
    spyOn(plugin, 'fireStop')
    plugin._adStopListener(obj)
    expect(plugin.fireStop).not.toHaveBeenCalled()
  })

  it('should be able to send stop from adstop, last postroll, with given', () => {
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.givenAds = 1
    plugin._adsAdapter = adsAdapter
    var obj = {
      data: {
        params: null
      }
    }
    spyOn(plugin, '_sendAdEventIfAllowed')
    spyOn(plugin, 'fireStop')
    plugin._adStopListener(obj)
    expect(plugin.fireStop).toHaveBeenCalled()
  })

  it('should not be able to send stop from adstop, not last postroll, with given', () => {
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.requestBuilder.lastSent.givenAds = 2
    plugin._adsAdapter = adsAdapter
    var obj = {
      data: {
        params: null
      }
    }
    spyOn(plugin, '_sendAdEventIfAllowed')
    spyOn(plugin, 'fireStop')
    plugin._adStopListener(obj)
    expect(plugin.fireStop).not.toHaveBeenCalled()
  })

  it('should be able to send stop from adstop, last postroll, with expected', () => {
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.options['ad.expectedPattern'] = {
      post: [1]
    }
    plugin._adsAdapter = adsAdapter
    var obj = {
      data: {
        params: null
      }
    }
    spyOn(plugin, '_sendAdEventIfAllowed')
    spyOn(plugin, 'fireStop')
    plugin._adStopListener(obj)
    expect(plugin.fireStop).toHaveBeenCalled()
  })

  it('should not be able to send stop from adstop, not last postroll, with expected', () => {
    plugin.requestBuilder.lastSent.position = 'post'
    plugin.options['ad.expectedPattern'] = {
      post: [2]
    }
    plugin._adsAdapter = adsAdapter
    var obj = {
      data: {
        params: null
      }
    }
    spyOn(plugin, '_sendAdEventIfAllowed')
    spyOn(plugin, 'fireStop')
    plugin._adStopListener(obj)
    expect(plugin.fireStop).not.toHaveBeenCalled()
  })
})
