describe('HlsTransform', () => {
  var HlsTransform = require('../../../../../../src/comm/transform/resourceparsers/formatparsers/hlsparser')
  var ht
  var hlsManifest = {
    getResponseText: function () {
      return '#EXTM3U\n' + 'file.ts'
    }
  }
  var otherManifest = {
    getResponseText: function () {
      return 'text<SegmentURL>\nmedia="http://segmentsample.com"text"""</SegmentURL>text'
    }
  }

  beforeEach(() => {
    ht = new HlsTransform()
  })

  it('should parse ts', () => {
    ht.parse('#EXT3\n' + 'file.ts')
    expect(ht.getResource()).toBe('file.ts')
  })

  it('should parse recursively, no result', () => {
    ht.parse('#EXT3\n' + 'file.m3u8')
    expect(ht.getResource()).toBeNull()
  })

  it('should parse recursively, with result', () => {
    ht.parse('#EXT3\n' + 'file.m3u8', hlsManifest)
    expect(ht.getResource()).toBe('file.ts')
  })

  it('should get nothing', () => {
    ht.parse('#EXT3\n' + 'file.abc')
    expect(ht.getResource()).toBeNull()
    expect(ht.getTransportFormat()).toBeNull()
  })

  it('should get mp4 transport format', () => {
    ht.parse('#EXT3\n' + 'file.mp4')
    expect(ht.getTransportFormat()).toBe('MP4')
  })

  it('should get mp4 transport format, with m4s', () => {
    ht.parse('#EXT3\n' + 'file.m4s')
    expect(ht.getTransportFormat()).toBe('MP4')
  })

  it('should get ts transport format', () => {
    ht.parse('#EXT3\n' + 'file.ts')
    expect(ht.getTransportFormat()).toBe('TS')
  })

  it('should parse ts, with parent resource', () => {
    ht.parse('#EXT3\n' + 'file.ts', null, 'http://fakeurl.com/manifest.m3u8')
    expect(ht.getResource()).toBe('http://fakeurl.com/file.ts')
  })

  it('should parse ts, with parent resource and full relative adress', () => {
    ht.parse('#EXTM3U\n' + '/route/file.ts', null, 'http://fakeurl.com/abcd/manifest.m3u8')
    expect(ht.getResource()).toBe('http://fakeurl.com/route/file.ts')
  })

  it('should accept to execute', () => {
    expect(ht.shouldExecute(hlsManifest)).toBe(true)
  })

  it('should refuse to execute', () => {
    expect(ht.shouldExecute(otherManifest)).toBe(false)
  })

  it('should execute, without manifest', () => {
    expect(ht.shouldExecute()).toBe(true)
  })
})
