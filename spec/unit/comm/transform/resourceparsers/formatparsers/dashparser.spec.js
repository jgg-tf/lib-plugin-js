describe('DashTransform', () => {
  var DashTransform = require('../../../../../../src/comm/transform/resourceparsers/formatparsers/dashparser')
  var dt

  var segmentUrlManifest = 'text<SegmentURL>media="http://segmentsample.com"text</SegmentURL>text'
  var segmentTemplateManifest = 'text<SegmentTemplate>media="http://templatesample.com"text</SegmentTemplate>text'
  var baseUrlManifest = 'text<BaseURL>http://baseurlsample.com</BaseURL>text' + segmentTemplateManifest
  var locationManifest = 'text<Location>http://sample.com</Location>text' + baseUrlManifest

  var segmentUrlManifest2 = 'text<SegmentURL>\nmedia="http://segmentsample.com"text"""</SegmentURL>text'
  var segmentTemplateManifest2 = 'text<SegmentTemplate>\nmedia="http://templatesample.com"text"""</SegmentTemplate>text'
  var baseUrlManifest2 = 'text<BaseURL>\nhttp://baseurlsample.com</BaseURL>text' + segmentTemplateManifest2
  var locationManifest2 = 'text<Location>\nhttp://sample.com</Location>text' + baseUrlManifest2

  var mp4 = locationManifest + '<AdaptationSet\n mimeType="video/mp4"\n'
  var mp4s = locationManifest + '<AdaptationSet\n mimeType="video/m4s"\n' 
  var ts = locationManifest + '<AdaptationSet\n mimeType="video/mp2t"\n'

  beforeEach(() => {
    dt = new DashTransform()
  })

  it('should parse nothing', () => {
    dt.parseFinalResource('', 'nothing')
    expect(dt.getResource()).toBe('nothing')
  })

  it('should parse segment url', () => {
    dt.parseFinalResource(segmentUrlManifest, '')
    expect(dt.getResource()).toBe('http://segmentsample.com')
    dt.parseFinalResource(segmentUrlManifest2, '')
    expect(dt.getResource()).toBe('http://segmentsample.com')
  })

  it('should parse segment template', () => {
    dt.parseFinalResource(segmentTemplateManifest, '')
    expect(dt.getResource()).toBe('http://templatesample.com')
    dt.parseFinalResource(segmentTemplateManifest2, '')
    expect(dt.getResource()).toBe('http://templatesample.com')
  })


  it('should parse segment template removing new line symbol', () => {
    var manifest = 'text<SegmentTemplate>\nmedia="http://templatesample.\ncom"text</SegmentTemplate>text'
    dt.parseFinalResource(manifest, '')
    expect(dt.getResource()).toBe('http://templatesample.com')
  })

  it('should parse base url', () => {
    dt.parseFinalResource(baseUrlManifest, '')
    expect(dt.getResource()).toBe('http://baseurlsample.com')
    dt.parseFinalResource(baseUrlManifest2, '')
    expect(dt.getResource()).toBe('http://baseurlsample.com')
  })

  it('should parse location', () => {
    spyOn(dt, 'parseFinalResource')
    dt.parse(locationManifest, '')
    dt.parse(locationManifest2, '')
    expect(dt.parseFinalResource).not.toHaveBeenCalled()
  })

  it('should parse location, inner method', () => {
    var manifest = {
      getResponseText: function () {
        return locationManifest
      }
    }
    spyOn(dt, 'parseFinalResource')
    dt.parseLocation(manifest, '')
    expect(dt.parseFinalResource).not.toHaveBeenCalled()
  })

  it('should not location, but fail and do nothing', () => {
    spyOn(dt, 'parse')
    spyOn(dt, 'parseFinalResource')
    dt.parseLocation(locationManifest, '')
    expect(dt.parseFinalResource).not.toHaveBeenCalled()
    expect(dt.parse).not.toHaveBeenCalled()
    expect(dt.getResource()).toBeNull()
  })

  it('should execute, without manifest', () => {
    expect(dt.shouldExecute()).toBe(true)
  })

  it('should parse transport format mp4, with mp4', () => {
    dt.parseFinalResource(mp4)
    expect(dt.getTransportFormat()).toBe('MP4')
  })

  it('should parse transport format mp4, with m4s', () => {
    dt.parseFinalResource(mp4s)
    expect(dt.getTransportFormat()).toBe('MP4')
  })

  it('should parse transport format ts', () => {
    dt.parseFinalResource(ts)
    expect(dt.getTransportFormat()).toBe('TS')
  })

  it('should not parse any transport format', () => {
    dt.parseFinalResource(locationManifest)
    expect(dt.getTransportFormat()).toBeNull()
  }) 
})
