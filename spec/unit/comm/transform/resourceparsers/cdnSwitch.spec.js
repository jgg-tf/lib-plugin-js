describe('CdnSwitch', () => {
  var CdnSwitch = require('../../../../../src/comm/transform/resourceparsers/cdnSwitch')
  var cdnSwitchObj

  beforeEach(() => {
    var pluginMock = {
      fakePlugin: true,
      options: {
        'parse.cdnTTL': 60
      },
      getResource: function () {
        return 'pluginres'
      },
      getAdapter: function (){
        return {
          getURLToParse: function () {
            return 'adapterres'
          },
          flags: {
            isStarted: true
          }
        }
      }
    }
    cdnSwitchObj = new CdnSwitch(pluginMock)
  })

  it('Should create cdnSwitch object, default values', () => {
    expect(cdnSwitchObj.headerName).toBe('x-cdn')
    expect(cdnSwitchObj.plugin.fakePlugin).toBeTrue()
  })

  it('Should trigger the success event', (done) => {
    cdnSwitchObj.on(CdnSwitch.Events.DONE, done())
    cdnSwitchObj.done()
  })

  it('Should trigger the failure event', (done) => {
    cdnSwitchObj.on(CdnSwitch.Events.ERROR, done())
    cdnSwitchObj.error()
  })

  it('Should not start the request without plugin ref', () => {
    var cdnSwitchObj2 = new CdnSwitch()
    spyOn(cdnSwitchObj2,'_request')
    expect(cdnSwitchObj2.plugin).not.toBeDefined()
    cdnSwitchObj2.init()
    expect(cdnSwitchObj2._request).not.toHaveBeenCalled()
  })

  it('Should start the request with a plugin ref', () => {
    spyOn(cdnSwitchObj,'_request')
    cdnSwitchObj.init()
    expect(cdnSwitchObj._request).toHaveBeenCalled()
  })

  it('Should not start the request without an adapter ref', () => {
    spyOn(cdnSwitchObj,'_request')
    cdnSwitchObj.plugin.getAdapter = function () {
      return null
    }
    cdnSwitchObj.init()
    expect(cdnSwitchObj._request).not.toHaveBeenCalled()
  })

  it('Should not start the request without a started view', () => {
    spyOn(cdnSwitchObj,'_request')
    cdnSwitchObj.plugin.getAdapter = function () {
      return {
        getURLToParse: function() {return 'abcd'},
        flags: {
          isStarted: false
        }
      }
    }
    cdnSwitchObj.init()
    expect(cdnSwitchObj._request).not.toHaveBeenCalled()
  })

  it('Should get getresource if urltoparse is null', () => {
    spyOn(cdnSwitchObj,'_request')
    cdnSwitchObj.plugin.getAdapter = function () {
      return {
        getURLToParse: function() {return null},
        flags: {
          isStarted: true
        }
      }
    }
    cdnSwitchObj.init()
    expect(cdnSwitchObj._request).toHaveBeenCalledWith('pluginres')
  })

  it('Should get urltoparse by default', () => {
    spyOn(cdnSwitchObj,'_request')
    cdnSwitchObj.init()
    expect(cdnSwitchObj._request).toHaveBeenCalledWith('adapterres')
  })

  it('Should parse a successful response', () =>{
    spyOn(cdnSwitchObj,'done')
    var resp = {
      getResponseHeaders: function (){
        return 'x-cdn: mycustomcdn \nabcd:abcd'
      }
    }
    cdnSwitchObj._successfulRequest(resp)
    expect(cdnSwitchObj.done).toHaveBeenCalledWith('mycustomcdn')
  })

  it('Should parse a successful empty response', () =>{
    spyOn(cdnSwitchObj,'done')
    var resp = {
      getResponseHeaders: function (){
        return 'abcd: abcd'
      }
    }
    cdnSwitchObj._successfulRequest(resp)
    expect(cdnSwitchObj.done).toHaveBeenCalledWith(null)
  })

  it('Should manage wrong request', () =>{
    spyOn(cdnSwitchObj,'error')
    cdnSwitchObj._failedRequest()
    expect(cdnSwitchObj.error).toHaveBeenCalled()
  })
})
