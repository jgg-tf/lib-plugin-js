describe('WrongTransform', () => {
  var WrongTransform = require('../../../../src/comm/transform/wrong')

  it('should block until start', () => {
    var v = new WrongTransform()
    var request = {
      params: {
        sessionId: 'asd'
      }
    }
    var requestWrong = {
      params: {
        key: 'value'
      }
    }
    expect(v.isBlocking(request)).toBe(false)
    expect(v.isBlocking(requestWrong)).toBe(true)
  })
})
