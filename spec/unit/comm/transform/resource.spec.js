describe('ResourceTransform', () => {
  var ResourceTransform = require('../../../../src/comm/transform/resource')
  var DashParser = require('../../../../src/comm/transform/resourceparsers/formatparsers/dashparser')
  var HlsParser = require('../../../../src/comm/transform/resourceparsers/formatparsers/hlsparser')
  var rt

  var infoA = {
    isParseManifest: function () { return true },
    isParseCdnNode: function () { return false },
    isCdnSwitch: function () { return false },
    getParseCdnNodeNameHeader: function () { return false },
    getParseCdnNodeList: function () { return [] }
  }
  var infoB = {
    isParseManifest: function () { return false },
    isParseCdnNode: function () { return true },
    isCdnSwitch: function () { return false },
    getParseCdnNodeNameHeader: function () { return true },
    getParseCdnNodeList: function () { return [] }
  }
  var dashManifest = {
    getResponseText: function () {
      return '<?xml version="1.0" encoding="UTF-8"?><MPD xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xml...' +
        '<BaseURL>http://baseurlsample.com</BaseURL>'
    },
    getResponseHeaders: function () {
      return ''
    }
  }

  beforeEach(() => { })

  it('should call parse CDN', () => {
    rt = new ResourceTransform(infoB)
    rt._cdnList = ['Amazon', 'Akamai']
    spyOn(rt, '_parseCDN')
    spyOn(rt, '_setTimeout')
    rt.init('a')
    expect(rt._parseCDN).toHaveBeenCalled()
    expect(rt._setTimeout).toHaveBeenCalled()
  })

  it('should return the initial url, ts case', () => {
    var url = 'https://abcd.com/abc/file.ts'
    rt = new ResourceTransform(infoA)
    spyOn(rt, 'parseManifest')
    rt.init(url)
    expect(rt.parseManifest).not.toHaveBeenCalled()
    expect(rt._realResource).toBe(url)
  })

  it('should call parse Dash', () => {
    rt = new ResourceTransform(infoA)
    var parser = new DashParser()
    expect(parser.shouldExecute(dashManifest)).toBeTrue
    rt.on('done', function () {
      expect(rt.getResource()).toBe('http://baseurlsample.com')
    })
    rt._parseManifest([parser], dashManifest)
  })

  it('should return the resource', () => {
    rt = new ResourceTransform(infoA)
    rt._initResource = 'asd'
    expect(rt.getResource()).toBe(null)
    rt._realResource = 'real'
    expect(rt.getResource()).toBe('real')
  })

  it('should do nothing without a resource', () => {
    rt = new ResourceTransform(infoA)
    rt.on('done', function () {
      expect(rt.getResource()).toBe(null)
    })
    rt.init()
  })
})
