describe('Chrono', () => {
  var Chrono = require('../../src/chrono.js')
  var chrono

  beforeEach(() => {
    chrono = new Chrono()
  })

  it('should start', () => {
    chrono.start()
    expect(chrono.startTime).toBeGreaterThan(-1)
    expect(chrono.stopTime).toBe(0)
  })

  it('should stop', () => {
    chrono.start()
    expect(chrono.stop()).toBeGreaterThan(-1)
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should stop with getDeltaTime(true)', () => {
    chrono.start()
    expect(chrono.getDeltaTime(true)).toBeGreaterThan(-1)
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should work if stop is called before start', () => {
    expect(chrono.stop()).toBe(-1)
    expect(chrono.getDeltaTime()).toBe(-1)
  })

  it('should work', (done) => {
    chrono.start()
    setTimeout(() => {
      expect(chrono.stop() / 1000).toBeCloseTo(1, 0)
      expect(chrono.getDeltaTime() / 1000).toBeCloseTo(1, 1)
      done()
    }, 1000)
  })

  it('should pause and resume the chronos', () => {
    chrono.start()
    chrono.pause()
    expect(chrono.pauseTime).not.toBeFalsy()
    chrono.resume()
    expect(chrono.pauseTime).toBeFalsy()
    expect(chrono.offset).toBeDefined()
  })

  it('should resume before stopping, if paused', () => {
    spyOn(chrono, 'resume')
    chrono.start()
    chrono.pause()
    expect(chrono.pauseTime).not.toBeFalsy()
    chrono.stop()
    expect(chrono.resume).toHaveBeenCalled()
  })

  it('should get delta from a non stopped, non paused chrono', () => {
    chrono.start()
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should get delta from a non stopped, paused chrono', () => {
    chrono.start()
    chrono.pause()
    expect(chrono.getDeltaTime()).toBeGreaterThan(-1)
  })

  it('should work with ATV timers', (done) => {
    atv = {
      setTimeout: setTimeout,
      clearTimeout: clearTimeout
    }
    setTimeout = null
    clearTimeout = null
    chrono.start()
    atv.setTimeout(() => {
      expect(chrono.stop() / 1000).toBeCloseTo(1, 0)
      expect(chrono.getDeltaTime() / 1000).toBeCloseTo(1, 1)
      setTimeout = atv.setTimeout
      clearTimeout = atv.clearTimeout
      delete atv
      done()
    }, 1000)
  })



  it('should do nothing without normal nor ATV timers', () => {
    var tmp = {
      setTimeout: setTimeout,
      clearTimeout: clearTimeout
    }
    atv = undefined
    setTimeout = null
    clearTimeout = null

      chrono.start()
      chrono.stop()
      chrono.getDeltaTime()

    setTimeout = tmp.setTimeout
    clearTimeout = tmp.clearTimeout
  })
})
