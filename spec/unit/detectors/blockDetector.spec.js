describe('Block detector', () => {
  var BlockDetector = require('../../../src/detectors/blockDetector')
  var plugin = {
    'options': {
      'app.https': false
    }
  }

  it('should not work, without navigator', () => {
    var detector = new BlockDetector(plugin)
    expect(detector.xhr).toBeDefined()
  })

  it('should get blocked by default', () => {
    var detector = new BlockDetector(plugin)
    expect(detector.isBlocked).toBeTruthy()
  })
})
