describe('Youboralib', () => {
  var youbora = require('../../src/youboralib')

  it('should define VERSION', () => {
    expect(youbora.VERSION).toBeDefined()
  })

  it('should noConflict', () => {
    var newlib = youbora.noConflict()

    expect(newlib).toBeDefined()
  })

  it('should register and unregister adapters', function () {
    youbora.registerAdapter('adapter', {})
    expect(youbora.adapters.adapter).toBeDefined()
    youbora.unregisterAdapter('adapter')
    expect(youbora.adapters.adapter).toBeNull()
  })
})
