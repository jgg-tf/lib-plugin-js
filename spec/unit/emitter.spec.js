describe('Emitter', () => {
  var Emitter = require('../../src/emitter.js')

  it('should emit and listen', (done) => {
    var emitter = new Emitter()
    emitter.on('event', () => {
      done()
    })
    emitter.emit('event')
  })

  it('should listen all', (done) => {
    var emitter = new Emitter()
    emitter.on('*', () => {
      done()
    })
    emitter.emit('special-event')
  })

  it('should remove listeners', (done) => {
    var emitter = new Emitter()
    var a = 0
    var cb = () => {
      a = 1
      done.fail()
    }

    emitter.on('a', cb)
    emitter.off('a', cb)
    emitter.emit('a')

    expect(a).toBe(0)
    done()
  })
})
