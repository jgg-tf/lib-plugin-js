/* eslint no-extend-native: "off", no-extra-bind: "off" */

describe('Polyfiller', () => {
  var objectCreate = require('../../src/mixins/create')

  beforeEach(() => {
    Function.prototype.bind = undefined
    Array.prototype.forEach = undefined
    Math.trunc = undefined
    require('../../src/polyfills')()
  })

  it('Function.prototype.bind', (done) => {
    setTimeout(function () {
      this()
    }.bind(done))
  })

  it('Array.forEach', () => {
    var arr = [0]
    arr.forEach((v) => {
      expect(v).toBe(0)
    })
  })

  it('Array.forEach should throw exception if not function', () => {
    expect(function () { [].forEach('not-a-function') }).toThrow()
  })

  it('Object.create should throw', () => {
    expect(() => { objectCreate(1, 2) }).toThrow()
    expect(() => { objectCreate(null) }).toThrow()
    expect(() => { objectCreate(1) }).toThrow()
  })

  it('Should use Math.trunc', () => {
    expect(Math.trunc(2.45)).toBe(2)
    expect(Math.trunc(NaN)).toBeNaN()
    expect(Math.trunc(-4.5)).toBe(-4)
    expect(Math.trunc(0)).toBe(0)
  })
})
