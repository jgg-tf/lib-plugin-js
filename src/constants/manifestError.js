/**
* List of ad manifest errors
*/
var ManifestError = {
  NO_RESPONSE: 'NO_RESPONSE',
  EMPTY: 'EMPTY_RESPONSE',
  WRONG: 'WRONG_RESPONSE'
}
module.exports = ManifestError
