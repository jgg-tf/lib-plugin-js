/**
 * List of ad positions
 */
var AdPosition = {
  Preroll: 'pre',
  Midroll: 'mid',
  Postroll: 'post'
}
module.exports = AdPosition
