/**
 * List of streaming protocols
 *   - HDS (Adobe HDS)
 *   - HLS (Apple HLS)
 *   - MSS (Microsoft Smooth Streaming)
 *   - DASH (MPEG-DASH)
 *   - RTMP (Adobe RTMP)
 *   - RTP (RTP)
 *   - RTSP (RTSP)
 */
var StreamingProtocol = {
  HDS: 'HDS',
  HLS: 'HLS',
  MSS: 'MSS',
  DASH: 'DASH',
  RTMP: 'RTMP',
  RTP: 'RTP',
  RTSP: 'RTSP'
}

module.exports = StreamingProtocol
