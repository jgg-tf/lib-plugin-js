var Log = require('../log')
var Constants = require('../constants')
var Adapter = require('../adapter/adapter')
var Util = require('../util')

// This file is designed to add extra functionalities to Plugin class

var PluginAdsMixin = {
  /**
     * Returns current adapter or null.
     *
     * @returns {Adapter}
     *
     * @memberof youbora.Plugin.prototype
     */
  getAdsAdapter: function () {
    return this._adsAdapter
  },

  /**
     * Sets an adapter for ads.
     *
     * @param {Adapter} adsAdapter
     *
     * @memberof youbora.Plugin.prototype
     */
  setAdsAdapter: function (adsAdapter) {
    if (adsAdapter.plugin) {
      Log.warn('Adapters can only be added to a single plugin')
    } else {
      this.removeAdsAdapter()
      adsAdapter.plugin = this
      this._adsAdapter = adsAdapter
      adsAdapter.setIsAds(true)
      this.adsAdapterListeners = {}
      this.adsAdapterListeners[Adapter.Event.START] = this._adStartListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.JOIN] = this._adJoinListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.PAUSE] = this._adPauseListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.RESUME] = this._adResumeListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.BUFFER_BEGIN] = this._adBufferBeginListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.BUFFER_END] = this._adBufferEndListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.STOP] = this._adStopListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.ERROR] = this._adErrorListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.CLICK] = this._adClickListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.MANIFEST] = this._adManifestListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.PODSTART] = this._adBreakStartListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.PODSTOP] = this._adBreakStopListener.bind(this)
      this.adsAdapterListeners[Adapter.Event.QUARTILE] = this._adQuartileListener.bind(this)
      for (var key in this.adsAdapterListeners) {
        this._adsAdapter.on(key, this.adsAdapterListeners[key])
      }
    }
  },

  /**
     * Removes the current adapter. Fires stop if needed. Calls adapter.dispose()
     *
     * @memberof youbora.Plugin.prototype
     */
  removeAdsAdapter: function () {
    if (this._adsAdapter) {
      this._adsAdapter.dispose()
      this._adsAdapter.plugin = null
      if (this.adsAdapterListeners) {
        for (var key in this.adsAdapterListeners) {
          this._adsAdapter.off(key, this.adsAdapterListeners[key])
        }
        delete this.adsAdapterListeners
      }
      this.resizeScrollDetector.stopDetection()
      this._adsAdapter = null
    }
  },

  // ---------------------------------------- LISTENERS -----------------------------------------
  _adStartListener: function (e) {
    if (this._adapter) {
      this._adapter.fireBufferEnd()
      this._adapter.fireSeekEnd()
      if (!this.isInitiated && !this._adapter.flags.isStarted) this._adapter.fireStart()
      if (this._adapter.flags.isPaused) this._adapter.chronos.pause.reset()
    } else {
      this.fireInit()
    }
    if (this._adsAdapter) {
      this._adsAdapter.chronos.viewedMax = []
    }
    var params = e.data.params || {}
    params.adNumber = this.requestBuilder.getNewAdNumber()
    var allParamsReady = (this.getAdResource() || this.getAdTitle()) && typeof this.getAdDuration() === 'number'
    if (allParamsReady) {
      this.adStartSent = true
      this._adsAdapter.fireManifest()
      this._adsAdapter.fireBreakStart()
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
    } else {
      this.adInitSent = true
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_INIT, Constants.Service.AD_INIT, params)
    }
  },

  _adJoinListener: function (e) {
    var params = {}
    Util.assign(params, e.data.params || {})
    if (this.adInitSent && !this.adStartSent) {
      this._adsAdapter.fireManifest()
      this._adsAdapter.fireBreakStart()
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_START, Constants.Service.AD_START, params)
    }
    this._adsAdapter.startChronoView()
    if (this.adConnected) {
      this._adsAdapter.chronos.join.startTime = this.adConnectedTime
      this._adsAdapter.chronos.total.startTime = this.adConnectedTime
      this.adConnectedTime = 0
      this.adConnected = false
    }
    params = e.data.params || {}
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_JOIN, Constants.Service.AD_JOIN, params)
    this.adInitSent = false
    this.adStartSent = false
  },

  _adPauseListener: function (e) {
    var params = e.data.params || {}
    this._adsAdapter.stopChronoView()
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_PAUSE, Constants.Service.AD_PAUSE, params)
  },

  _adResumeListener: function (e) {
    var params = e.data.params || {}
    this._adsAdapter.startChronoView()
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_RESUME, Constants.Service.AD_RESUME, params)
  },

  _adBufferBeginListener: function (e) {
    Log.notice('Ad Buffer Begin')
    this._adsAdapter.stopChronoView()
    if (this._adsAdapter && this._adsAdapter.flags.isPaused) {
      this._adsAdapter.chronos.pause.reset()
    }
  },

  _adBufferEndListener: function (e) {
    var params = e.data.params || {}
    this._adsAdapter.startChronoView()
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_BUFFER, Constants.Service.AD_BUFFER, params)
  },

  _adStopListener: function (e) {
    this._adsAdapter.stopChronoView()
    this._adsAdapter.flags.reset()
    this._totalPrerollsTime = (this._totalPrerollsTime || 0) + this._adsAdapter.chronos.total.getDeltaTime()

    var params = e.data.params || {}
    params.position = this.requestBuilder.lastSent.position
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_STOP, Constants.Service.AD_STOP, params)

    // If its a stop for a postroll we check if we can detect if its the last one to call view stop
    if (this.requestBuilder.lastSent.position === Constants.AdPosition.Postroll) {
      var pat = this.options['ad.expectedPattern']
      this.playedPostrolls++
      // If we know the amount of postrolls and this was the last one
      if ((this.requestBuilder.lastSent.givenAds && this.requestBuilder.lastSent.givenAds <= this.playedPostrolls) ||
      // Or if we have expected (and not given!) and this was the last expected one
      (!this.requestBuilder.lastSent.givenAds && pat && pat.post && pat.post[0] && pat.post[0] <= this.playedPostrolls)) {
        this.fireStop()
      }
    }

    this.adConnected = true
    this.adConnectedTime = new Date().getTime()
  },

  _adErrorListener: function (e) {
    var params = e.data.params || {}
    if (this._adapter && !this._adapter.flags.isStarted && !this.isInitiated) {
      this._savedAdError = e
      return null // Ignore ad errors before content
    }
    if (this._blockAdError(e.data.params)) return null
    if (this._adsAdapter) {
      this._adsAdapter.fireManifest()
      this._adsAdapter.fireBreakStart()
    }
    if (!this._adsAdapter || !this._adsAdapter.flags.isStarted) {
      params.adNumber = this.requestBuilder.getNewAdNumber()
    }
    if (!this.isBreakStarted) {
      params.breakNumber = this.requestBuilder.getNewBreakNumber()
    }
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_ERROR, Constants.Service.AD_ERROR, params)
  },

  _adSavedError: function () {
    if (this._savedAdError) {
      this._adErrorListener(this._savedAdError)
      this._savedAdError = null
    }
  },

  _adSavedManifest: function () {
    if (this._savedAdManifest) {
      this._adManifestListener(this._savedAdManifest)
      this._savedAdManifest = null
    }
  },

  _blockAdError: function (errorParams) {
    var now = Date.now()
    var sameError = this._lastAdErrorParams &&
      this._lastAdErrorParams.errorCode === errorParams.errorCode &&
      this._lastAdErrorParams.msg === errorParams.msg &&
      this._lastAdErrorParams.adCreativeId === this.getAdCreativeId()

    if (sameError && this._lastAdErrorTime + 5000 > now) {
      this._lastAdErrorTime = now
      return true
    }
    this._lastAdErrorTime = now
    this._lastAdErrorParams = errorParams
    return false
  },

  _adClickListener: function (e) {
    var params = e.data.params || {}
    this._adsAdapter.stopChronoView()
    this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_CLICK, Constants.Service.AD_CLICK, params)
  },

  _adManifestListener: function (e) {
    if (!this.isAdsManifestSent) {
      if (this._adapter && !this._adapter.flags.isStarted && !this.isInitiated) {
        this._savedAdManifest = e
        return null // Ignore ad manifest before content
      }
      var params = e.data.params || {}
      this.isAdsManifestSent = true
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_MANIFEST, Constants.Service.AD_MANIFEST, params)
    }
  },

  _adBreakStartListener: function (e) {
    if (!this.isBreakStarted) {
      this.isBreakStarted = true
      var params = e.data.params || {}
      params.breakNumber = this.requestBuilder.getNewBreakNumber()
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_POD_START, Constants.Service.AD_POD_START, params)
      this.adConnected = false
    }
  },

  _adBreakStopListener: function (e) {
    if (this.isBreakStarted) {
      this.isBreakStarted = false
      var params = e.data.params || {}
      params.position = this.requestBuilder.lastSent.position
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_POD_STOP, Constants.Service.AD_POD_STOP, params)
      this.adConnected = false
    }
  },

  _adQuartileListener: function (e) {
    var params = e.data.params || {}
    if (params.quartile) {
      this._sendAdEventIfAllowed(Constants.WillSendEvent.WILL_SEND_AD_QUARTILE, Constants.Service.AD_QUARTILE, params)
    }
  },

  _sendAdEventIfAllowed: function (willSend, service, params) {
    if (!this.options['ad.ignore']) this._send(willSend, service, params)
    Log.notice(service)
  }

}

module.exports = PluginAdsMixin
