// This file is designed to add extra functionalities to Plugin class

var PluginInfinityGettersMixin = {
  getContext: function () {
    var ret = 'Default'
    if (this.options['session.context']) {
      ret = this.storage.getSession('context')
    }
    return ret
  },

  getScrollDepth: function () {
    var ret = this.storage.getSession('pageScrollDepth')
    this.storage.removeSession('pageScrollDepth')
    return ret
  },

  getSession: function () {
    return this.storage.getStorages('session')
  },

  getStorageHost: function () {
    return this.storage.getStorages('host')
  },

  getStoredData: function () {
    return this.storage.getStorages('data')
  },

  getDataTime: function () {
    return this.storage.getStorages('dataTime')
  },

  getLastActive: function () {
    return this.storage.getStorages('lastactive')
  },

  setStoredData: function (data) {
    this.storage.setStorages('data', data)
  },

  setSession: function (session) {
    this.storage.setStorages('session', session)
  },

  setDataTime: function (time) {
    this.storage.setStorages('dataTime', time)
  },

  setLastActive: function (last) {
    this.storage.setStorages('lastactive', last)
  },

  getPageName: function () {
    if (typeof document !== 'undefined' && document.title) {
      return document.title
    }
  },

  getPageLoadTime: function () {
    return this.browserLoadTimes.getPageLoadTime()
  },

  getIsSessionExpired: function () {
    var now = new Date().getTime()
    return !this.getSession() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  },

  getIsDataExpired: function () {
    var now = new Date().getTime()
    return !this.storage.isEnabled() || !this.getStoredData() || (this.infinity.getFirstActive() < now - this.sessionExpire)
  }
}

module.exports = PluginInfinityGettersMixin
