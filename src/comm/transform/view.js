var YBRequest = require('../request')
var Transform = require('./transform')
var Log = require('../../log')
var Util = require('../../util')
var Constants = require('../../constants')

var ViewTransform = Transform.extend(
  /** @lends youbora.ViewTransform.prototype */
  {
    /**
     * This class manages Fastdata service and view index.
     *
     * @constructs
     * @extends youbora.Transform
     * @memberof youbora
     *
     * @param {Plugin} plugin Instance of {@link Plugin}
     * @param {string} session If provided, plugin will use this as a FD response.
     */
    constructor: function (plugin, session) {
      Transform.prototype.constructor.apply(this, arguments)

      /** /data response */
      this.response = {}

      this._viewIndex = new Date().getTime()

      this._session = session

      this._httpSecure = plugin.options['app.https']

      this._plugin = plugin

      this.transformName = 'View'
    },

    /**
     * Starts the 'FastData' fetching. This will send the initial request to YOUBORA in order to get
     * the needed info for the rest of the requests.
     *
     * This is an asynchronous process.
     *
     * When the fetch is complete, {@link #fastDataConfig} will contain the parsed info.
     * @see FastDataConfig
     */
    init: function () {
      // offline
      if (this._plugin.options && this._plugin.options.offline) {
        // set the options
        this.response.host = 'OFFLINE'
        this.response.code = 'OFFLINE'
        this.response.pingTime = 60
        this.response.beatTime = 60
        this.done()
        return null
      }

      // reusing old data not expired
      if (this._plugin.storage.isEnabled()) {
        var now = new Date().getTime()
        if (now < this._plugin.sessionExpire + (Number(this._plugin.getDataTime()) || 0) &&
          this._plugin.getStoredData()) {
          this.setData(this._plugin.getStoredData())
          return null
        }
      }

      // request new data
      var service = Constants.Service.DATA
      var params = {
        outputformat: 'json'
      }

      params = this._plugin.requestBuilder.buildParams(params, service)
      if (params !== null) {
        Log.notice(service + ' ' + params.system)
        if (params.system === 'nicetest') {
          // "nicetest" is the default accountCode.
          // If found here, it's very likely that the customer has forgotten to set it.
          Log.error(
            'No accountCode has been set. Please set your accountCode inside plugin\'s options.'
          )
        }

        new YBRequest(this._plugin.getHost(), service, params)
          .on(YBRequest.Event.SUCCESS, this._receiveData.bind(this))
          .on(YBRequest.Event.ERROR, this._failedData.bind(this))
          .send()
      }
    },

    /**
     * Uses given response to set fastdata response.
     *
     * @param {String} response Fastdata response as json string.
     */
    setData: function (response) {
      try {
        var resp = JSON.parse(response)
        if (this._plugin.options['parse.fdsResponseHost']) {
          response = response.replace(resp.q.h, this._plugin.options['parse.fdsResponseHost'](resp.q.h))
          resp = JSON.parse(response)
        }
        this.response.msg = response
        this.response.host = Util.addProtocol(resp.q.h, this._httpSecure)
        this.response.code = resp.q.c
        this.response.pingTime = resp.q.pt || 5
        this.response.beatTime = resp.q.i ? resp.q.i.bt || 30 : 30
        this.response.sessionExpire = resp.q.i ? resp.q.i.exp || 300 : 300
        this._plugin.storage.setLocal('sessionExpire', this.response.sessionExpire)
        this.done()
      } catch (err) {
        Log.error('Fastdata response is invalid.')
      }
    },

    /**
     * Parse the response from the fastData service.
     *
     * @private
     */
    _receiveData: function (req, e) {
      var msg = req.getResponse()
      this.setData(msg)
    },

    _failedData: function (req, e) {
      Log.error('Fastdata request has failed.')
    },

    /**
     * This method will increment the view index (timestamp values). The library handles this
     * automatically, but some error flow might need calling this manually.
     * @return {string} new viewcode
     */
    nextView: function () {
      this._viewIndex = new Date().getTime()
      return this.getViewCode()
    },

    /**
     * Returns current viewcode
     * @return {string} viewcode
     */
    getViewCode: function () {
      return this.response.code + '_' + this._viewIndex
    },

    /**
     * Returns the current session
     *
     * @returns {string} Session
     */
    getSession: function () {
      return this._session
    },

    /**
     * Sets the session
     *
     * @param {String} sessionId Sets the session
     */
    setSession: function (session) {
      this._session = session
    },

    /** Sets the host url
    *
    * @param {String} host Sets the host
    */
    setHost: function (host) {
      this.response.host = host
    },

    /**
     * Transform requests
     * @param {youbora.comm.YBRequest} request YBRequest to transform.
     */
    parse: function (request) {
      // Host, system and sessionRoot for all
      request.host = request.host || this.response.host
      request.params.system = this._plugin.getAccountCode()
      request.params.sessionRoot = request.params.sessionRoot || this.getSession()

      // PingTime for ping and start
      if (ViewTransform.EventList.PingTime.indexOf(request.service) !== -1) {
        request.params.pingTime = request.params.pingTime || this.response.pingTime
      }

      // ViewCode for non infinity events
      if (ViewTransform.EventList.Infinity.indexOf(request.service) === -1) {
        request.params.code = request.params.code || this.getViewCode()
      } else {
        // For infinity events, sessionId
        request.params.sessionId = request.params.sessionRoot
      }

      // For init error and start, parentId and navContext
      if (ViewTransform.EventList.CreateView.indexOf(request.service) !== -1) {
        if (this._plugin.infinity.infinityStarted || (this._plugin.storage.isEnabled() && this._plugin.storage.getLocal('infinityStarted'))) {
          request.params.parentId = request.params.sessionRoot
          request.params.navContext = request.params.navContext || this._plugin.getContext()
        }
      }
    }
  },
  {
    // Static members
    EventList: {
      CreateView: [
        Constants.Service.START,
        Constants.Service.INIT,
        Constants.Service.ERROR
      ],
      Infinity: [
        Constants.Service.NAV,
        Constants.Service.SESSION_START,
        Constants.Service.SESSION_STOP,
        Constants.Service.EVENT,
        Constants.Service.BEAT
      ],
      PingTime: [
        Constants.Service.START,
        Constants.Service.PING
      ]
    }
  })

module.exports = ViewTransform
