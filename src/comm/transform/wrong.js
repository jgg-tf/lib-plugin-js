var Transform = require('./transform')

/**
 * This transform ensures that no requests will be sent without sessionId when is required
 *
 * @constructs
 * @extends youbora.Transform
 * @memberof youbora
 * @name WrongTransform
 *
 * @param {Plugin} plugin Instance of {@link Plugin}
 */
var WrongTransform = Transform.extend(
  /** @lends youbora.WrongTransform.prototype */
  {
    /**
   * Returns if transform is blocking.
   *
   * @param {YBRequest} request Request to transform.
   * @return {bool} True if queue shall be blocked.
   */
    isBlocking: function (request) {
      if (!request.params.sessionId) {
        return true
      }
      return false
    }
  }
)

module.exports = WrongTransform
