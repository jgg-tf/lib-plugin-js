var AdPosition = require('./constants/adPosition')
var Adapter = require('./constants/adapter')
var ManifestError = require('./constants/manifestError')
var Service = require('./constants/service')
var WillSendEvent = require('./constants/willSendEvent')
var AdInsertionType = require('./constants/adInsertionType')
/**
 * This static class englobes youbora constants.
 *
 * @class
 * @static
 * @memberof youbora
 */
var Constants = {
  AdPosition: AdPosition,
  ManifestError: ManifestError,
  Service: Service,
  WillSendEvent: WillSendEvent,
  AdInsertionType: AdInsertionType,
  Adapter: Adapter
}

module.exports = Constants
