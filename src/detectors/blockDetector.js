var YouboraObject = require('../object')
var Util = require('../util')
var YBRequest = require('../comm/request')

var BlockDetector = YouboraObject.extend({
  constructor: function (plugin) {
    this.isBlocked = true
    try {
      this.xhr = YBRequest.prototype.createXHR()
      if (this.xhr.addEventListener) {
        var url = Util.addProtocol('ww.zxcvwwds.com/ww/wwds/-imwwge-wwd_wwds.html', plugin.options['app.https'])
        this.xhr.open('GET', url.replace(/ww/g, 'a'), true)
        this.xhr.addEventListener('load', function () {
          this.isBlocked = false
        }.bind(this))
        this.xhr.send()
      }
    } catch (err) {
      // this.isBlocked = true
    }
  }
})

module.exports = BlockDetector
