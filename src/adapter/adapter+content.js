var AdapterConstants = require('../constants/adapter')
var Log = require('../log')

var AdapterContentMixin = {
  /** Override to return current playrate */
  getPlayrate: function () {
    return !this.flags.isPaused ? 1 : 0
  },

  /** Override to return Frames Per Secon (FPS) */
  getFramesPerSecond: function () {
    return null
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return null
  },

  /** Override to return user bandwidth throughput */
  getThroughput: function () {
    return null
  },

  /** Override to return rendition */
  getRendition: function () {
    return null
  },

  /** Override to return title2 */
  getTitle2: function () {
    return null
  },

  /** Override to recurn true if live and false if VOD */
  getIsLive: function () {
    return null
  },

  /** Override to return CDN traffic bytes not using streamroot or peer5. */
  getCdnTraffic: function () {
    return null
  },

  /** Override to return P2P traffic bytes not using streamroot or peer5. */
  getP2PTraffic: function () {
    return null
  },

  /** Override to return P2P traffic sent in bytes, not using streamroot or peer5. */
  getUploadTraffic: function () {
    return null
  },

  /** Override to return if P2P is enabled not using streamroot or peer5. */
  getIsP2PEnabled: function () {
    return null
  },
  /** Override to return household id */
  getHouseholdId: function () {
    return null
  },

  /** Override to return the latency */
  getLatency: function () {
    return null
  },

  /** Override to return the number of packets lost */
  getPacketLoss: function () {
    return null
  },

  /** Override to return the number of packets sent */
  getPacketSent: function () {
    return null
  },

  /** Override to return a json with metrics */
  getMetrics: function () {
    return null
  },

  /** Override to return the audio codec */
  getAudioCodec: function () {
    return null
  },

  /** Override to return the video codec */
  getVideoCodec: function () {
    return null
  },

  /** Override to return a chunk or intermediate manifest url */
  getURLToParse: function () {
    return null
  },

  /**
 * Emits related event and set flags if current status is valid.
 * ie: won't sent start if isStarted is already true.
 *
 * @param {Object} [params] Object of key:value params to add to the request.
 * @param {bool} [convertFromBuffer=true] If false, will convert current buffer to seek.
 */
  fireSeekBegin: function (params, convertFromBuffer) {
    if (this.plugin && this.plugin.getIsLive() && this.plugin.options['content.isLive.noSeek']) return null
    if (this.flags.isJoined && !this.flags.isSeeking) {
      if (this.flags.isBuffering) {
        if (convertFromBuffer !== false) {
          Log.notice('Converting current buffer to seek')

          this.chronos.seek = this.chronos.buffer.clone()
          this.chronos.buffer.reset()

          this.flags.isBuffering = false
        } else {
          return
        }
      } else {
        this.chronos.seek.start()
      }

      this.flags.isSeeking = true
      this.emit(AdapterConstants.Event.SEEK_BEGIN, { params: params })
    }
  },

  /**
   * Emits related event and set flags if current status is valid.
   * ie: won't sent start if isStarted is already true.
   *
   * @param {Object} [params] Object of key:value params to add to the request.
   */
  fireSeekEnd: function (params) {
    if (this.plugin && this.plugin.getIsLive() && this.plugin.options['content.isLive.noSeek']) return null
    if (this.flags.isJoined && this.flags.isSeeking) {
      this.cancelSeek()
      this.emit(AdapterConstants.Event.SEEK_END, { params: params })
    }
  },

  /**
   *
   * @param {Object} [params] Object of key:value params to add to the request.
   */
  cancelSeek: function (params) {
    if (this.flags.isJoined && this.flags.isSeeking) {
      this.flags.isSeeking = false

      this.chronos.seek.stop()

      if (this.monitor) this.monitor.skipNextTick()
    }
  },

  /**
 * Emits event request.
 *
 * @param {Object} [params] Object of key:value params to add to the request.
 */
  fireEvent: function (eventName, dimensions, values, topLevelDimensions) {
    var returnparams = topLevelDimensions || {}
    returnparams.name = eventName || ''
    returnparams.dimensions = dimensions || {}
    returnparams.values = values || {}
    this.emit(AdapterConstants.Event.VIDEO_EVENT, { params: returnparams })
  }
}

module.exports = AdapterContentMixin
